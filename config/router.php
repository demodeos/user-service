<?php
declare(strict_types=1);

/**
 *  [
'method'=>'POST',
'pattern'=>'v1/diplomacity/koss/<guid:[0-9a-zA-Z-]{30,40}>',
'controller'=>\App\Modules\officers\controllers\OfficersController::class,
'action'=>'saveKossList'
],
 */


return [
  'routes'=>[...require_once __DIR__.'/routes/users.php']

];
