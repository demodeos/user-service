<?php
declare(strict_types=1);


return [
    [
        'method'=>'GET',
        'pattern'=>'v1/main/test',
        'controller'=>\web\v1\test\TestController::class,
        'action'=>'actionStart'
    ],
    [
        'method'=>'POST',
        'pattern'=>'v1/main/test',
        'controller'=>\web\v1\test\TestController::class,
        'action'=>'actionStart'
    ],


];