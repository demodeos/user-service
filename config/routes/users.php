<?php
declare(strict_types=1);


return [
    [
        'method'=>'GET',
        'pattern'=>'v1/auth',
        'controller'=>\web\v1\main\MainController::class,
        'action'=>'actionLogin'
    ],

    [
        'method'=>'POST',
        'pattern'=>'v1/auth',
        'controller'=>\web\v1\main\MainController::class,
        'action'=>'actionRegistration'
    ],

    [
        'method'=>'PUT',
        'pattern'=>'v1/auth>',
        'controller'=>\web\v1\main\MainController::class,
        'action'=>'actionLogin'
    ],


    [
        'method'=>'DELETE',
        'pattern'=>'v1/auth>',
        'controller'=>\web\v1\main\MainController::class,
        'action'=>'actionLogout'
    ],


];