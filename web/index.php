<?php
define("APP", __DIR__.'/../');

require_once __DIR__.'/../vendor/autoload.php';
$config = require_once __DIR__.'/../config/config.php';

require_once "function.php";


\Demodeos\Api\Core::init($config);
